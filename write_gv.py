#!/usr/bin/python3

#GPL3 by David Hamner
import subprocess

#runs apt-rdepends -d <all .deb on system> and writes to ./out.gv
def write_deps_as_gv():
    installed_stuff = run_cmd("dpkg -l")
    package_list_start_found = False
    return_data = {}
    rdepends_cmd = "apt-rdepends -d "
    for package in installed_stuff.split('\n'):
        #cut off top of command above ========
        if "=======" in package:
            package_list_start_found = True
            continue
        if not package_list_start_found:
            continue
        
        #cut off end of command
        if package == "":
            continue
        
        #process package string
        raw_package_data = package.split()
        name = raw_package_data[1]
        if ":amd64" in name:
            name = name[:-6]
        version = raw_package_data[2]
        return_data[name] = version
        rdepends_cmd = rdepends_cmd + f"{name} "
        #print(f"{name} ", end="")
    gv_data = run_cmd(rdepends_cmd)
    with open("./out.gv", "w+") as fh:
        fh.write(gv_data)
    return(return_data)

#returns list of needed packages
def get_dependencies(package):
    rdepends = run_cmd(f"apt-rdepends {package}")
    print(rdepends)
    exit()


#Run cmd then return std out
def run_cmd(cmd):
    std_out = subprocess.check_output(cmd.split(" "))
    std_out = std_out.decode()
    return(std_out)

#adds too package and package_links
packages = []
package_links = []
def process_dot_line(line):
    #print(f"CMD: {line}")
    global packages
    global package_links
    #We don't need node or graph cmds
    if line.startswith("node") or line.startswith("graph"):
        return()
    if "->" in line:
        try:
            cmd_data = line.split('"')

            #print(cmd_data)
            if "pos" not in line:
                from_package, to_package = line.split(" -> ")
                pos = []
            else:
                pos = cmd_data[-2]
                #print(f"Debug {cmd_data}")
                from_package = line.split("->")[0]
                to_package = line.split("[")[0].split()[-1]
            
            from_package = from_package.strip('" ')
            to_package = to_package.strip('" ')
            package_links.append([from_package, to_package, pos])
            #print(f"Append link: {[from_package, to_package, pos]}")
        except Exception:
            cmd_data = line.split('"')
            from_package = cmd_data[1]
            to_package = cmd_data[3]
            pos = cmd_data[5]
            package_links.append([from_package, to_package, pos])
            #print(f"Append link2: {[from_package, to_package, pos]}")
    else:
        if not "\t[" in line:
            cmd_data = line.split('"')
            print(cmd_data)
            try:
                package = cmd_data[1]
            except Exception:
                print(f"Error reading: {line}")
                return(1)
            pos = cmd_data[3]
            packages.append([package,pos])
            #print(f"Append1: {[package,pos]}")
        else:
            package = line.split('[')[0].strip()
            package = package.strip('"')
            pos = line.split('pos="')[-1].split('"')[0]
            packages.append([package,pos])
            #print(f"Append: {[package,pos]}")
    

write_deps_as_gv()

#installed_packages = write_deps_as_gv()
#raw_dependencies = {}
"""
for package in installed_packages.keys():
    deps = get_dependencies(package)
    raw_dependencies[package] = deps
"""
