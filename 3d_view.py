
#GPL3 by David Hamner

import os
from random import *
import bpy
import bmesh
import string
import time
import subprocess

dot_data_file = "/home/david/myProjects/3d_dependency_tree/librem_with_pos.gv"
#dot_data_file = "/home/david/myProjects/3d_dependency_tree/subset_with_pos.gv"
#dot_data_file = "/home/david/myProjects/3d_dependency_tree/out_with_pos.gv"

#cleanup
try:
    bpy.ops.object.mode_set(mode='EDIT')
    for material in bpy.data.materials:
        material.user_clear()
        bpy.data.materials.remove(material)
        bpy.ops.object.mode_set(mode='OBJECT')
except RuntimeError:
    print("started empty")
bpy.ops.mesh.primitive_plane_add(size=2, enter_editmode=False, align='WORLD', location=(0, 0, 0))
bpy.ops.object.mode_set(mode='OBJECT')
bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False)
#bpy.context.space_data.display_mode = 'ORPHAN_DATA'



room_size = 20
max_depth = 30

max_doors = 20
Z = 10
change = 2

#Use to override num_dir_door to ROOM_OVERRIDE
SIMPLE_ROOM_TEST = False
ROOM_OVERRIDE = 1

rendered_rooms = {}
paths_rendered = {}

#don't render files past limit_depth for limit_files_on
limit_files_on = ["/sys", "/var"]
limit_depth = 3


colors = {"/dev":    (255, 0, 0),
          "/etc":    (0, 255, 255),
          "/var":    (0, 0, 255),
          "/bin":    (0, 0, 160),
          "/boot":   (255, 90, 0),
          "/home":   ( 128, 0, 128 ),
          "/lib":    (255, 255, 0 ),
          "/media":  ( 0, 255, 0),
          "/mnt":    (128, 0, 0),
          "/opt":    (128, 128, 0),
          "/proc":   (0, 80, 0),
          "/root":   (255, 0, 255),
          "/run":    (165, 42, 42),
          "/sbin":   (255, 128, 64),
          "/srv":    (128, 128, 128),
          "/sys":    (106, 27, 154),
          "/tmp":    (0,0,0),
          "/usr":    (255, 255, 0),
          "/var":    (0, 0, 128)}

def add_link(name, from_pos, to_pos):
    bpy.ops.object.select_all(action='DESELECT')
    from_pos = from_pos
    to_pos = to_pos
    diff_pos = [to_pos[0]-from_pos[0],to_pos[1]-from_pos[1],to_pos[2]-from_pos[2]]
    #print(from_pos)
    bpy.ops.mesh.primitive_circle_add(enter_editmode=False, align='WORLD', location=from_pos)
    bpy.ops.object.editmode_toggle()
    bpy.ops.transform.rotate(value=1.5708, orient_axis='Y')
    
    bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"use_normal_flip":False, "mirror":False})
    
    
    bpy.ops.transform.translate(value=diff_pos)
    
    bpy.ops.transform.rotate(value=1.5708, orient_axis='Y')


    
    bpy.ops.object.editmode_toggle()



#setup root
def setup_room(offset=(0,0,0), root=False):
    #bpy.ops.object.mode_set(mode='OBJECT')
    #bpy.ops.object.select_all(action='SELECT')
    bpy.ops.object.select_all(action='DESELECT')
    bpy.ops.mesh.primitive_cube_add(size=room_size, location=offset)
    #ob = bpy.context.object # get the active object for example
    #mesh = ob.data
    #Enter edit mode to extrude

    #bpy.ops.object.mode_set(mode='EDIT')
    #bpy.ops.mesh.normals_make_consistent(inside=False)

    #bm = bmesh.from_edit_mesh(mesh)
    #for face in bm.faces:
    #    face.select = False
    #bm.faces[1].select = True


def exists(path):
    """Test whether a path exists.  Returns False for broken symbolic links"""
    try:
        os.stat(path)
    except OSError:
        return False
    return True

def set_color(name, color):
    #print(color)
    if len(color) == 3:
        r, g, b = color
    elif len(color) == 4:
        r, g, b, a = color
    r = r/255
    g = g/255
    b = b/255
    color = (r,g,b, 1)
    ob = bpy.context.object # get active mesh
    m = ob.data
    activeObject = bpy.context.active_object
    mat = bpy.data.materials.new(name=name + str(color))
    mat.diffuse_color = color
    #set new material to variable
    activeObject.data.materials.append(mat) #add the material to the object
    #print(dir(bpy.context.object.active_material))
        
def build_room(room_name, pos=(0,0,0), color=(0,0,0)):
    global Z
    global change
    global rendered_rooms
    global paths_rendered
    
    x,y,z = pos
    offset=(x,y,z)
    setup_room(offset=offset)
    rendered_rooms[room_name] = offset

    #set color
    set_color(room_name, color)
    
    #set name:
    obj = bpy.context.active_object
    obj.name = room_name
    obj.data.name = room_name
    

def name_rooms(rendered_rooms):
    for roomname in rendered_rooms:
        print(f"Adding room name: {roomname}")
        pos = rendered_rooms[roomname]
        #print(local_roomname, pos)
        #setup text
        text_pos = [pos[0],pos[1],pos[2]]
        bpy.ops.object.text_add(enter_editmode=False, align='WORLD', location=text_pos)
        ob=bpy.context.object
        ob.data.body = roomname
        
        #Grow text and move
        bpy.ops.transform.resize(value=(6,6,6))
        bpy.ops.transform.translate(value=(7, -12.0, -0))


        #bpy.ops.transform.resize(value=(0.34535, 0.34535, 0.34535), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
        
        #flip x 90
        bpy.ops.transform.rotate(value=1.5708, orient_axis='X', orient_type='LOCAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='LOCAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
        
        #center
        bpy.ops.transform.translate(value=(-15.4443, -0, -0), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)

        #flip y 180
        #bpy.ops.transform.rotate(value=3.14159, orient_axis='Y', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, True, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)

        set_color(roomname, (1,1,1))
        #move up:
        #bpy.ops.transform.translate(value=(0, 0, ((height/2)+.01)*-1), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, True, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)

def add_files(rendered_rooms):
    for roomname in rendered_rooms:
        pos = rendered_rooms[roomname]
        #setup offset:
        pos = [pos[0],pos[1]-(room_size*.24),pos[2]+(room_size*.038)]
        if roomname.startswith(root_dir):
            local_roomname = roomname.split(root_dir)[-1]
        else:
            local_roomname = roomname
        files_to_show = []
        try:
            files_and_dirs = os.listdir(roomname)
        except Exception:
            files_and_dirs = []
        
        continue_loop = True
        for dirs_to_limit in limit_files_on:
            if local_roomname.startswith(dirs_to_limit):
                if len(local_roomname.split("/")) > limit_depth:
                    continue_loop = False
        if not continue_loop:
            print(f"Removed {local_roomname} from render")
            continue
        
        for file_or_dir in files_and_dirs:
            file_or_dir = roomname + "/" + file_or_dir
            if exists(file_or_dir) and not os.path.isdir(file_or_dir):
                if not os.path.islink(file_or_dir):
                    #print(file_or_dir)
                    files_to_show.append(file_or_dir)
        
        index = 0
        size_multipler = 0.4
        last_size = 0
        max_in_row = 42
        og_y = pos[1]
        for file_name in files_to_show:
            

            size = os.path.getsize(file_name)
            if size == 0:
                size = 1
            height = (size_multipler/20000000)*size
            height = 1*height
            if height > size_multipler:
                height = size_multipler
            if index % max_in_row == 0:
                pos[1] = og_y
                pos = [pos[0] + size_multipler/2,pos[1],pos[2]]
            else:
                pos = [pos[0],pos[1] + size_multipler/2,pos[2]]
            print(f"{file_name} pos:{index} size:{height}")
            #bpy.ops.mesh.primitive_plane_add(enter_editmode=False, align='WORLD', location=pos, scale=(size_multipler, size_multipler, size_multipler))
            
            #add cube file
            bpy.ops.mesh.primitive_cube_add(size=1, enter_editmode=False, align='WORLD', location=pos, scale=(size_multipler, size_multipler, height*2))
            
            obj = bpy.context.active_object
            obj.name = file_name
            obj.data.name = file_name
            
            
            #setup text
            text_pos = [pos[0],pos[1],pos[2]]
            bpy.ops.object.text_add(enter_editmode=False, align='WORLD', location=text_pos, scale=(1, 1, 1))
            ob=bpy.context.object
            ob.data.body = f"./{file_name.split('/')[-1]}"
            #shrink text
            bpy.ops.transform.resize(value=(0.0184535, 0.0184535, 0.0184535), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
            
            #flip x 90
            bpy.ops.transform.rotate(value=1.5708, orient_axis='X', orient_type='LOCAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='LOCAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
            
            #flip y 180
            bpy.ops.transform.rotate(value=3.14159, orient_axis='Y', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, True, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)


            #move up:
            bpy.ops.transform.translate(value=(0, 0, ((height/2)+.01)*-1), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, True, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
            
            if file_name.startswith(root_dir):
                file_name = file_name.split(root_dir)[-1]
            
            #setup text color
            set_color(file_name, (1,1,1))

            bpy.ops.object.select_all(action='DESELECT')
            index = index + 1

#flip wold 180 over y
def flip_it():
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.transform.rotate(value=3.14159, orient_axis='Y', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, True, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)

#rendered_rooms={'/home/david/myProjects/librem5_image_builder/tmp/boot': [29.060606002807617, 58.0, 0.0454545140266418]}
#rendered_rooms={'/home/david/myProjects/librem5_image_builder/tmp': (0, 0, 0), '/home/david/myProjects/librem5_image_builder/tmp/home': [-145.48484802246094, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/media': [145.4242401123047, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/opt': [126.03028869628906, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/dev': [106.6363525390625, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/sys': [87.24241638183594, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/run': [67.84848022460938, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/srv': [48.45454406738281, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/boot': [29.060606002807617, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/root': [9.666666984558105, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/usr': [-9.727272987365723, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/lost+found': [-29.1212100982666, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/mnt': [-48.51515197753906, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/etc': [-67.90908813476562, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/tmp': [-87.30302429199219, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/var': [-106.69696044921875, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/proc': [-126.09089660644531, 58.0, 0.0454545140266418]}

#Non-blender fuctions
#for building debian tree
#=========================================================
#=========================================================
#=========================================================
#runs apt-rdepends -d <all .deb on system> and writes to ./out.gv
def write_deps_as_gv():
    installed_stuff = run_cmd("dpkg -l")
    package_list_start_found = False
    return_data = {}
    rdepends_cmd = "apt-rdepends -d "
    for package in installed_stuff.split('\n'):
        #cut off top of command above ========
        if "=======" in package:
            package_list_start_found = True
            continue
        if not package_list_start_found:
            continue
        
        #cut off end of command
        if package == "":
            continue
        
        #process package string
        raw_package_data = package.split()
        name = raw_package_data[1]
        if ":amd64" in name:
            name = name[:-6]
        version = raw_package_data[2]
        return_data[name] = version
        rdepends_cmd = rdepends_cmd + f"{name} "
        #print(f"{name} ", end="")
    gv_data = run_cmd(rdepends_cmd)
    with open("./out.gv", "w+") as fh:
        fh.write(gv_data)
    return(return_data)

#returns list of needed packages
def get_dependencies(package):
    rdepends = run_cmd(f"apt-rdepends {package}")
    print(rdepends)
    exit()


#Run cmd then return std out
def run_cmd(cmd):
    std_out = subprocess.check_output(cmd.split(" "))
    std_out = std_out.decode()
    return(std_out)

#adds too package and package_links
packages = []
pos_map = {}
depth_map = {}
package_links = []
def process_dot_line(line):
    #print(f"CMD: {line}")
    global packages
    global package_links
    #We don't need node or graph cmds
    if line.startswith("node") or line.startswith("graph"):
        return()
    if "->" in line:
        try:
            cmd_data = line.split('"')

            #print(cmd_data)
            if "pos" not in line:
                from_package, to_package = line.split(" -> ")
                pos = []
            else:
                pos = cmd_data[-2]
                #print(f"Debug {cmd_data}")
                from_package = line.split("->")[0]
                to_package = line.split("[")[0].split()[-1]
            
            from_package = from_package.strip('" ')
            if "\t" in from_package:
                from_package = from_package.split("\t")[0]
            to_package = to_package.strip(';" ')
            if "\t" in to_package:
                to_package = to_package.split("\t")[0]
            #pos = pos + ",0"
            package_links.append([from_package, to_package, pos])
            if to_package in depth_map:
                depth_map[to_package] = depth_map[to_package] + 1
            else:
                depth_map[to_package] = 1
            print(f"Append link: {[from_package, to_package, pos]}")
        except Exception:
            cmd_data = line.split('"')
            from_package = cmd_data[1]
            if "\t" in from_package:
                from_package = from_package.split("\t")[0]
            to_package = cmd_data[3]
            if "\t" in to_package:
                to_package = to_package.split("\t")[0]
            pos = cmd_data[5]
            #pos = pos + ",0"
            package_links.append([from_package, to_package, pos])
            if to_package in depth_map:
                depth_map[to_package] = depth_map[to_package] + 1
            else:
                depth_map[to_package] = 1
            #print(f"Append link2: {[from_package, to_package, pos]}")
    else:
        if not "\t[" in line:
            cmd_data = line.split('"')
            #print(cmd_data)
            try:
                package = cmd_data[1]
            except Exception:
                print(f"Error reading: {line}")
                return(1)
            pos = cmd_data[3]
            packages.append([package,pos])
            pos_map[package] = pos

            print(f"Append1: {[package,pos]}")
        else:
            package = line.split('[')[0].strip()
            package = package.strip('"')
            pos = line.split('pos="')[-1].split('"')[0]
            packages.append([package,pos])
            pos_map[package] = pos

            #print(f"Append: {[package,pos]}")
    
#file_name = "./subset_with_pos.gv"
#file_name = "./out_with_pos.gv"
with open(dot_data_file) as fh:
    raw_dot_data = fh.readlines()


line_buffer = ""
for line in raw_dot_data:
    line = line.strip()
    if line.endswith("{") or line.endswith("}"):
        continue
    
    if not line.endswith(",") and not line.startswith("size="):
        line_buffer = line_buffer + line
        process_dot_line(line_buffer)
        line_buffer = ""
    else:
        line_buffer = line_buffer + line
    
def get_depth(name):
    global depth_map
    global known_heights
    global layer_map
    
    layer_size = 20
    
    #print(f"Package: {name}")
    num_deps = depth_map[name]
    #print(f"num_deps: {num_deps}")
    layer = layer_map[num_deps]
    #print(f"Layer: {layer}")
    return(layer * layer_size)

layer_map = {}
known_heights = []
for dep in depth_map:
    known_heights.append(depth_map[dep])
layer = 0
max_depth = max(known_heights)
for layer_depth in range(0,max_depth+1):
    if layer_depth in known_heights:
        layer_map[layer_depth] = layer
        layer = layer + 1


#print(depth_map)
print(max_depth)
#print(known_heights)
print(layer_map)

#print(packages)
print("\nPackages:")
for package in packages:
    name, pos = package
    pos = pos.split(",")
    pos = [float(pos[0]),float(pos[1])]
    if name in depth_map:
        pos = pos + [get_depth(name)]
    else:
        pos = pos + [0]
    print(pos)
    build_room(name,pos=pos, color=(0, 255, 255))
    #print(package)
    #print(f"Line: {line}")

print("\nLinks:")
for link in package_links:
    print(link)
    from_package, to_package, pos_raw = link
    if pos_raw == []:
        continue
    if to_package not in pos_map:
        continue
    if from_package not in pos_map:
        continue
    pos_data = pos_raw.split(" ")
    name = f"{from_package} -> {to_package}"
    from_pos = pos_map[from_package].split(",")
    from_pos = [float(from_pos[0]),float(from_pos[1])]
    if from_package in depth_map:
        from_pos = from_pos + [get_depth(from_package)]
    else:
        from_pos = from_pos + [0]
    
    
    to_pos = pos_map[to_package].split(",")
    to_pos = [float(to_pos[0]),float(to_pos[1])]
    to_pos = pos_map[to_package].split(",")
    to_pos = [float(to_pos[0]),float(to_pos[1])]
    if to_package in depth_map:
        to_pos = to_pos + [get_depth(to_package)]
    else:
        to_pos = to_pos + [0]
    
    #print(f"?{from_pos} -> {to_pos}")
    add_link(name, from_pos, to_pos)

print(depth_map)
#============================================================
#============================================================
#============================================================


#build_room("bob", pos=[40.5,148.6])
#build_room("bob2", pos=[625.54,18])
#print(rendered_rooms)
bpy.ops.object.mode_set(mode='OBJECT')
name_rooms(rendered_rooms)

#setup files in rendered_rooms
bpy.ops.object.mode_set(mode='OBJECT')
#add_files(rendered_rooms)
#name_rooms(rendered_rooms)
#name_paths(paths_rendered)
#flip_it()#Lol
#build_room("/1", ["1","2","3"], first=False)
#for door_that_needs_room_index in range(0, num_dir_door):
    
#bpy.ops.mesh.delete(type='FACE')
#bm.faces[-1].select = True
#bm.faces[-3].select = True
#bm.faces[-5].select = True
#bm.faces[-7].select = True
#bpy.ops.mesh.delete(type='FACE')


